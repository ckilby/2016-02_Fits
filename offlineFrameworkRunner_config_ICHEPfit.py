# Config file to define a list of inputs to run through ttH-offline, with associated ttH-offline options,
# to be given to OfflineFrameworkRunner
#
# OfflineFrameworkRunner expects this config to:
#   * create a dict called inputDict
#   * fill inputDict with ProcessParams objects, with the key as the inputType of the corresponding ProcessParams
#
# Every element of the dict will be used as a single ttH-offline run

# Creating inputDict
inputDict = {}

# Probably just put this bit in a systematics file and use the SystematicsFromFile option
nomTree = 'nominal_Loose'
allSystTrees = ''
# allSystTrees = 'EG_RESOLUTION_ALL__1down_Loose,EG_RESOLUTION_ALL__1up_Loose,EG_SCALE_ALL__1down_Loose,EG_SCALE_ALL__1up_Loose,JET_19NP_JET_BJES_Response__1down_Loose,JET_19NP_JET_BJES_Response__1up_Loose,JET_19NP_JET_EffectiveNP_1__1down_Loose,JET_19NP_JET_EffectiveNP_1__1up_Loose,JET_19NP_JET_EffectiveNP_2__1down_Loose,JET_19NP_JET_EffectiveNP_2__1up_Loose,JET_19NP_JET_EffectiveNP_3__1down_Loose,JET_19NP_JET_EffectiveNP_3__1up_Loose,JET_19NP_JET_EffectiveNP_4__1down_Loose,JET_19NP_JET_EffectiveNP_4__1up_Loose,JET_19NP_JET_EffectiveNP_5__1down_Loose,JET_19NP_JET_EffectiveNP_5__1up_Loose,JET_19NP_JET_EffectiveNP_6restTerm__1down_Loose,JET_19NP_JET_EffectiveNP_6restTerm__1up_Loose,JET_19NP_JET_EtaIntercalibration_Modelling__1down_Loose,JET_19NP_JET_EtaIntercalibration_Modelling__1up_Loose,JET_19NP_JET_EtaIntercalibration_NonClosure__1down_Loose,JET_19NP_JET_EtaIntercalibration_NonClosure__1up_Loose,JET_19NP_JET_EtaIntercalibration_TotalStat__1down_Loose,JET_19NP_JET_EtaIntercalibration_TotalStat__1up_Loose,JET_19NP_JET_Flavor_Composition__1down_Loose,JET_19NP_JET_Flavor_Composition__1up_Loose,JET_19NP_JET_Flavor_Response__1down_Loose,JET_19NP_JET_Flavor_Response__1up_Loose,JET_19NP_JET_Pileup_OffsetMu__1down_Loose,JET_19NP_JET_Pileup_OffsetMu__1up_Loose,JET_19NP_JET_Pileup_OffsetNPV__1down_Loose,JET_19NP_JET_Pileup_OffsetNPV__1up_Loose,JET_19NP_JET_Pileup_PtTerm__1down_Loose,JET_19NP_JET_Pileup_PtTerm__1up_Loose,JET_19NP_JET_Pileup_RhoTopology__1down_Loose,JET_19NP_JET_Pileup_RhoTopology__1up_Loose,JET_19NP_JET_PunchThrough_MC15__1down_Loose,JET_19NP_JET_PunchThrough_MC15__1up_Loose,JET_19NP_JET_SingleParticle_HighPt__1down_Loose,JET_19NP_JET_SingleParticle_HighPt__1up_Loose,JET_JER_SINGLE_NP__1up_Loose,MET_SoftTrk_ResoPara_Loose,MET_SoftTrk_ResoPerp_Loose,MET_SoftTrk_ScaleDown_Loose,MET_SoftTrk_ScaleUp_Loose,MUON_ID__1down_Loose,MUON_ID__1up_Loose,MUON_MS__1down_Loose,MUON_MS__1up_Loose,MUON_SCALE__1down_Loose,MUON_SCALE__1up_Loose'

nomPlusAllSystTrees = nomTree#+','+allSystTrees

defaultConfig    = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike.txt'
fakesOnlyConfig  = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_fakesOnly.txt'
promptOnlyConfig = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_promptOnly.txt'

dataConfig       = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_data.txt'

nomOnlySysts = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_systsNomOnly.txt'
allSysts = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_systsAll.txt'
allSysts_forAFII = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_systsAll_forAFII.txt'

pathToInputs = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offFrameworkInputs/'

# Filling inputDict with desired inputs
inputDict['data']                     = ProcessParams(inputType = 'data',
                                                      configFile = dataConfig,
                                                      inputList = pathToInputs+'inputs_data.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

####################################################################################################################################

inputDict['ttH_aMcP8_FS']             = ProcessParams(inputType = 'ttH_aMcP8_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttH_aMcP8_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['ttH_aMcHpp_FS']            = ProcessParams(inputType = 'ttH_aMcHpp_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttH_aMcHpp_FS.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

####################################################################################################################################

inputDict['ttbar_PP8_lep_FS']         = ProcessParams(inputType = 'ttbar_PP8_lep_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_lep_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_dil_FS']         = ProcessParams(inputType = 'ttbar_PP8_dil_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_dil_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_merge_FS']       = ProcessParams(inputType = 'ttbar_PP8_merge_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_merge_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {'DoTTbarBfilter':'true', 'DoTTbarDileptonFilter':'true'})

inputDict['ttbar_PP8_lep_AFII']       = ProcessParams(inputType = 'ttbar_PP8_lep_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_lep_AFII.txt',
                                                      systFile = allSysts_forAFII,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_lep_bfil_AFII'] = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_AFII.txt',
                                                      systFile = allSysts_forAFII,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_dil_AFII']       = ProcessParams(inputType = 'ttbar_PP8_dil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_dil_AFII.txt',
                                                      systFile = allSysts_forAFII,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PP8_dil_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_dil_bfil_AFII.txt',
                                                      systFile = allSysts_forAFII,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8_merge_AFII']     = ProcessParams(inputType = 'ttbar_PP8_merge_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8_merge_AFII.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {'DoTTbarBfilter':'true', 'DoTTbarDileptonFilter':'true'})

####################################################################################################################################

inputDict['ttbar_PP8up_lep_AFII']     = ProcessParams(inputType = 'ttbar_PP8up_lep_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8up_lep_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PP8down_lep_AFII']   = ProcessParams(inputType = 'ttbar_PP8down_lep_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP8down_lep_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PH7_lep_AFII']       = ProcessParams(inputType = 'ttbar_PH7_lep_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PH7_lep_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PH7_lep_bfil_AFII'] = ProcessParams(inputType = 'ttbar_PH7_lep_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PH7_lep_bfil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PH7_dil_AFII']       = ProcessParams(inputType = 'ttbar_PH7_dil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PH7_dil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PH7_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PH7_dil_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PH7_dil_bfil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_PH7_merge_AFII']     = ProcessParams(inputType = 'ttbar_PH7_merge_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PH7_merge_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {'DoTTbarBfilter':'true', 'DoTTbarDileptonFilter':'true'})

inputDict['ttbar_aMcP8_lep_AFII']     = ProcessParams(inputType = 'ttbar_aMcP8_lep_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_aMcP8_lep_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_lep_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_bfil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_aMcP8_dil_AFII']    = ProcessParams(inputType = 'ttbar_aMcP8_dil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_aMcP8_dil_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_dil_bfil_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_bfil_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

inputDict['ttbar_aMcP8_merge_AFII']   = ProcessParams(inputType = 'ttbar_aMcP8_merge_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcP8_merge_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {'DoTTbarBfilter':'true', 'DoTTbarDileptonFilter':'true'})

####################################################################################################################################

inputDict['ttbar_PP6_FS']             = ProcessParams(inputType = 'ttbar_PP6_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP6_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {'DoTTbarBfilter':'true', 'DoTTbarDileptonFilter':'true'})

####################################################################################################################################

inputDict['ttbar_PP6radHi_FS']        = ProcessParams(inputType = 'ttbar_PP6radHi_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP6radHi_FS.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttbar_PP6radLo_FS']        = ProcessParams(inputType = 'ttbar_PP6radLo_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP6radLo_FS.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttbar_PP6_AFII']           = ProcessParams(inputType = 'ttbar_PP6_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PP6_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttbar_PHpp_AFII']          = ProcessParams(inputType = 'ttbar_PHpp_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_PHpp_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttbar_aMcHpp_AFII']        = ProcessParams(inputType = 'ttbar_aMcHpp_AFII',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttbar_aMcHpp_AFII.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})
                                      
inputDict['diboson_FS']               = ProcessParams(inputType = 'diboson_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_diboson_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['Wtprompt_FS']              = ProcessParams(inputType = 'Wtprompt_FS',
                                                      configFile = promptOnlyConfig,
                                                      inputList = pathToInputs+'inputs_Wtchan_dilepPrompt_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['Wtfakes_FS']            =   ProcessParams(inputType = 'Wtfakes_FS',
                                                     configFile = fakesOnlyConfig,
                                                     inputList = pathToInputs+'inputs_Wtchan_inclFakes_FS.txt',
                                                     systFile = allSysts,
                                                     extraOptionsDict = {})
                                      
inputDict['schan_FS']                 = ProcessParams(inputType = 'schan_FS',
                                                   configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_schan_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['tchan_FS']                 = ProcessParams(inputType = 'tchan_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tchan_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttZ_FS']                   = ProcessParams(inputType = 'ttZ_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttZ_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['ttW_FS']                   = ProcessParams(inputType = 'ttW_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_ttW_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['4top_FS']                  = ProcessParams(inputType = '4top_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tttt_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['tV_FS']                    = ProcessParams(inputType = 'tV_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tV_FS.txt',
                                                      systFile = allSysts,
                                                     extraOptionsDict = {})
                                      
inputDict['tWZ_FS']                   = ProcessParams(inputType = 'tWZ_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tWZ_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['tWW_FS']                   = ProcessParams(inputType = 'tWW_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tWW_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['zjets_FS']                 = ProcessParams(inputType = 'zjets_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_zjets_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
                                      
inputDict['wjets_FS']                 = ProcessParams(inputType = 'wjets_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_wjets_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})
