SampleType data
Normalisation cutFlow
Regions ge3jge2b:"nJets >=3 && nBTags70 >= 2",3j2b:"nJets == 3 && nBTags70 == 2",3j3b:"nJets == 3 && nBTags70 == 3",ge4j2b:"nJets >= 4 && nBTags70 == 2",ge4j3b:"nJets >= 4 && nBTags70 == 3",ge4jge4b:"nJets >= 4 && nBTags70 >= 4"
VariableListToDecorate /scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_variableList.txt
SaveAllDecorations true
SaveDefaultVariables true
SaveSystematicWeights true
UseLargeJets false
WeightsFile /scratch4/ckilby/ttH_Work/AnalysisTop-2.4.25/2016-02_Fits/offlineConfig_ICHEPlike_weights.txt
